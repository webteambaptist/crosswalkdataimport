namespace GLXWALKDataImport.Models
{
    using System.Data.Entity;

    public partial class XWalkModel : DbContext
    {
        public XWalkModel()
            : base("name=XWalkModel")
        {
        }

        public virtual DbSet<C_GLXWALK_B> C_GLXWALK_B { get; set; }
        public virtual DbSet<C_GLXWALK_B_Archive> C_GLXWALK_B_Archive { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.OldOrg)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
               .Property(e => e.OldOrgDesc)
               .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.OldDept)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.OldDeptDesc)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.OldAcct)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.OldDesc)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.OldAccountCode)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.NewOrg)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.NewOrgDesc)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.NewDept)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.DeptDesc)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.NewAcct)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.NewDesc)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.NewAccountCode)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B>()
                .Property(e => e.NewAcctType)
                .IsUnicode(false);


            //modelBuilder.Entity<C_GLXWALK_B>()
            //    .MapToStoredProcedures(p => p.Delete(sp => sp.HasName("delXwalk"))
            //);

            //modelBuilder.Entity<C_GLXWALK_B_Archive>()
            //     .MapToStoredProcedures(p => p.Insert(sp => sp.HasName("archiveXwalk"))
            //                    .Delete(sp => sp.HasName("delXwalkArchive"))
            //);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.OldOrg)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
               .Property(e => e.OldOrgDesc)
               .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.OldDept)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.OldDeptDesc)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.OldAcct)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.OldDesc)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.OldAccountCode)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.NewOrg)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.NewOrgDesc)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.NewDept)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.DeptDesc)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.NewAcct)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.NewDesc)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.NewAccountCode)
                .IsUnicode(false);

            modelBuilder.Entity<C_GLXWALK_B_Archive>()
                .Property(e => e.NewAcctType)
                .IsUnicode(false);

        }
    }
}
