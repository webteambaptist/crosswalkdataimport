namespace GLXWALKDataImport.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("_GLXWALK_B_Archive")]
    public partial class C_GLXWALK_B_Archive
    {
        public int ID { get; set; }

        [StringLength(100)]
        public string OldOrg { get; set; }

        [StringLength(200)]
        public string OldOrgDesc { get; set; }

        [StringLength(100)]
        public string OldDept { get; set; }

        [StringLength(200)]
        public string OldDeptDesc { get; set; }

        [StringLength(100)]
        public string OldAcct { get; set; }

        [StringLength(200)]
        public string OldDesc { get; set; }

        [StringLength(100)]
        public string OldAccountCode { get; set; }

        [StringLength(100)]
        public string NewOrg { get; set; }

        [StringLength(200)]
        public string NewOrgDesc { get; set; }

        [StringLength(100)]
        public string NewDept { get; set; }

        [StringLength(200)]
        public string DeptDesc { get; set; }

        [StringLength(100)]
        public string NewAcct { get; set; }

        [StringLength(200)]
        public string NewDesc { get; set; }

        [StringLength(100)]
        public string NewAccountCode { get; set; }

        [StringLength(5)]
        public string NewAcctType { get; set; }
    }
}
