﻿using GLXWALKDataImport.Models;
using OfficeOpenXml;
using System;
using System.Configuration;
using System.IO;
//using System.Web.Http;
using System.Net.Mail;

namespace GLXWALKDataImport
{
    class Program
    {
        static void Main(string[] args)
        {
            var incomingPath = ConfigurationManager.AppSettings["INCOMINGFILESPATH"];
            var archivePath = ConfigurationManager.AppSettings["ARCHIVEFILESPATH"];
            var originPath = ConfigurationManager.AppSettings["FILEORIGINPATH"];
            var failPath = ConfigurationManager.AppSettings["FILEFAILPATH"];
            var fileName = ConfigurationManager.AppSettings["FILENAME"];
            var worksheetName = ConfigurationManager.AppSettings["WORKSHEETNAME"];
            var environment = ConfigurationManager.AppSettings["Environment"];

            var rowCount = 0;
            var formatCorrect = true;

            String fullOriginPath = originPath + fileName + ".xlsx";
            String fullFilePath = incomingPath + fileName + ".xlsx";
            String fullArchivePath = archivePath + fileName + DateTime.UtcNow.ToBinary().ToString() + ".xlsx";

            FileInfo originatingFile = new FileInfo(fullOriginPath);
            if (originatingFile.Exists)//check for file in shared location
            {
                System.IO.DirectoryInfo incDireInfo = new DirectoryInfo(incomingPath);

                //delete any file still in the incoming directory
                foreach (FileInfo file in incDireInfo.GetFiles())
                {
                    File.Move(file.FullName, failPath + file.Name.Replace(file.Extension, "") + DateTime.UtcNow.ToBinary().ToString() + file.Extension);
                }

                File.Copy(fullOriginPath, fullFilePath);//copy the file to web server
            }

            FileInfo existingFile = new FileInfo(fullFilePath);

            //check file exists
            if (existingFile.Exists)
            {
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {

                    ExcelWorksheet worksheet = package.Workbook.Worksheets[worksheetName];
                    if (worksheet != null)
                    {
                        int oldOrgIndex = worksheet.GetColumnByName("OldOrg");
                        //check column names
                        if (oldOrgIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int oldOrgDescIndex = worksheet.GetColumnByName("OldOrgDesc");
                        if (oldOrgDescIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int oldDeptIndex = worksheet.GetColumnByName("OldDept");
                        if (oldDeptIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int oldDeptDescIndex = worksheet.GetColumnByName("OldDeptDesc");
                        if (oldDeptDescIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int oldAcctIndex = worksheet.GetColumnByName("OldAcct");
                        if (oldAcctIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int oldDescIndex = worksheet.GetColumnByName("OldAcctDesc");
                        if (oldDescIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int oldAccountCodeIndex = worksheet.GetColumnByName("OldAccountCode");
                        if (oldAccountCodeIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int newOrgIndex = worksheet.GetColumnByName("NewOrg");
                        if (newOrgIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int newOrgDescIndex = worksheet.GetColumnByName("NewOrgDesc");
                        if (newOrgDescIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int newDeptIndex = worksheet.GetColumnByName("NewDept");
                        if (newDeptIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int deptDescIndex = worksheet.GetColumnByName("NewDeptDesc");
                        if (deptDescIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int newAcctIndex = worksheet.GetColumnByName("NewAcct");
                        if (newAcctIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int newDescIndex = worksheet.GetColumnByName("NewDesc");
                        if (newDescIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int newAccountCodeIndex = worksheet.GetColumnByName("NewAccountCode");
                        if (newAccountCodeIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        int newAcctTypeIndex = worksheet.GetColumnByName("NewAcctType");
                        if (newAcctTypeIndex == -99)
                        {
                            formatCorrect = false;
                        }

                        if (formatCorrect)
                        {
                            using (var context = new XWalkModel())
                            {
                                context.Configuration.AutoDetectChangesEnabled = false;
                                //delete archive records
                                int archiveRecordsDeleted = context.Database.ExecuteSqlCommand("EXEC delXwalkArchive");

                                //archive all current records
                                int recordsArchived = context.Database.ExecuteSqlCommand("EXEC archiveXwalk");

                                //delete all current records
                                int currentRecordsDeleted = context.Database.ExecuteSqlCommand("EXEC delXwalk");

                                //add records from new workbook
                                for (int row = 2; row <= worksheet.Dimension.End.Row; row++)
                                {
                                    //Console.WriteLine("\tCell({0},{1}).Value={2}", row, col, worksheet.Cells[row, col].Value);

                                    var newRow = new C_GLXWALK_B();
                                    if (worksheet.Cells[row, oldOrgIndex].Value != null)
                                    {
                                        newRow.OldOrg = worksheet.Cells[row, oldOrgIndex].Value.ToString();
                                    }
                                    if (worksheet.Cells[row, oldOrgDescIndex].Value != null)
                                    {
                                        newRow.OldOrgDesc = worksheet.Cells[row, oldOrgDescIndex].Value.ToString().ToUpper();
                                    }
                                    if (worksheet.Cells[row, oldDeptIndex].Value != null)
                                    {
                                        newRow.OldDept = worksheet.Cells[row, oldDeptIndex].Value.ToString();
                                    }
                                    if (worksheet.Cells[row, oldDeptDescIndex].Value != null)
                                    {
                                        newRow.OldDeptDesc = worksheet.Cells[row, oldDeptDescIndex].Value.ToString().ToUpper();
                                    }
                                    if (worksheet.Cells[row, oldAcctIndex].Value != null)
                                    {
                                        newRow.OldAcct = worksheet.Cells[row, oldAcctIndex].Value.ToString();
                                    }
                                    if (worksheet.Cells[row, oldDescIndex].Value != null)
                                    {
                                        newRow.OldDesc = worksheet.Cells[row, oldDescIndex].Value.ToString().ToUpper();
                                    }
                                    if (worksheet.Cells[row, oldAccountCodeIndex].Value != null)
                                    {
                                        newRow.OldAccountCode = worksheet.Cells[row, oldAccountCodeIndex].Value.ToString();
                                    }
                                    if (worksheet.Cells[row, newOrgIndex].Value != null)
                                    {
                                        newRow.NewOrg = worksheet.Cells[row, newOrgIndex].Value.ToString();
                                    }
                                    if (worksheet.Cells[row, newOrgDescIndex].Value != null)
                                    {
                                        newRow.NewOrgDesc = worksheet.Cells[row, newOrgDescIndex].Value.ToString().ToUpper();
                                    }
                                    if (worksheet.Cells[row, newDeptIndex].Value != null)
                                    {
                                        newRow.NewDept = worksheet.Cells[row, newDeptIndex].Value.ToString();
                                    }
                                    if (worksheet.Cells[row, deptDescIndex].Value != null)
                                    {
                                        newRow.DeptDesc = worksheet.Cells[row, deptDescIndex].Value.ToString().ToUpper();
                                    }
                                    if (worksheet.Cells[row, newAcctIndex].Value != null)
                                    {
                                        newRow.NewAcct = worksheet.Cells[row, newAcctIndex].Value.ToString();
                                    }
                                    if (worksheet.Cells[row, newDescIndex].Value != null)
                                    {
                                        newRow.NewDesc = worksheet.Cells[row, newDescIndex].Value.ToString().ToUpper();
                                    }
                                    if (worksheet.Cells[row, newAccountCodeIndex].Value != null)
                                    {
                                        newRow.NewAccountCode = worksheet.Cells[row, newAccountCodeIndex].Value.ToString();
                                    }
                                    if (worksheet.Cells[row, newAcctTypeIndex].Value != null)
                                    {
                                        newRow.NewAcctType = worksheet.Cells[row, newAcctTypeIndex].Value.ToString();
                                    }

                                    context.C_GLXWALK_B.Add(newRow);

                                    if (rowCount % 5000 == 0)
                                    {
                                        context.SaveChanges();
                                        Console.WriteLine("Rows inserted:" + rowCount.ToString());
                                    }
                                    rowCount++;

                                }

                                context.SaveChanges();
                                Console.WriteLine("Rows inserted:" + rowCount.ToString());
                            }
                        }
                        else
                        {
                            Console.WriteLine("Spreadsheet is in the wrong format");
                            //send email that the format was wrong
                            SendEmail(fileName + ".xlxs did not load as it was in an incorrect format. Load date and time: " + DateTime.Now.ToString(), environment + " - GLXWALK file in incorrect format");
                        }
                    }
                }
                if (formatCorrect)
                {
                    System.IO.DirectoryInfo di = new DirectoryInfo(archivePath);

                    foreach (FileInfo file in di.GetFiles())
                    {
                        if (file.LastWriteTime < DateTime.Now.AddDays(-7))
                        {
                            file.Delete();
                        }
                    }

                    File.Move(fullFilePath, fullArchivePath);

                    //send email that the file loaded successfully
                    SendEmail(rowCount.ToString() + " rows loaded successfully. Load date and time: " + DateTime.Now.ToString(), environment + " - GLXWALK File loaded successfully");
                }
            }
            Console.WriteLine();
            Console.WriteLine("Process complete");
            Console.WriteLine();
        }

        static void SendEmail(string Body, string Subject)
        {

            SmtpClient smtpClient = new SmtpClient();
            MailMessage message = new MailMessage();

            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            try
            {
                smtpClient.Host = ConfigurationManager.AppSettings["RELAY"];
                smtpClient.Port = 25;
                smtpClient.EnableSsl = false;

                MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["sender"]);
                message.From = fromAddress;

                var sentTo = ConfigurationManager.AppSettings["recipient"];

                string[] sTO = sentTo.Split(Convert.ToChar(","));
                for (int i = 0; i < sTO.Length; i++)
                {
                    message.To.Add(sTO[i]);
                }

                message.Subject = Subject;
                message.IsBodyHtml = true;
                message.Body = Body;


                smtpClient.Send(message);
            }
            catch (Exception ex)
            {

            }

        }
    }
}

