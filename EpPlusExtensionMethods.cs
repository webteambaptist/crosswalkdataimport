﻿using OfficeOpenXml;
using System;
using System.Linq;

namespace GLXWALKDataImport
{
    public static class EpPlusExtensionMethods
    {
        public static int GetColumnByName(this ExcelWorksheet ws, string columnName)
        {
            int result = 0;
            if (ws == null)
            {
                throw new ArgumentNullException(nameof(ws));
            }

            try
            {
                result = ws.Cells["1:1"].First(c => c.Value.ToString() == columnName).Start.Column;
            }
            catch
            {
                result = -99;
            }



            return result;
        }
    }
}
